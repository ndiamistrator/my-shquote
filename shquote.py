#!/bin/env -S python -I

# ndiamistrator -- 2022-06-18

'''
    Various shell quoters
'''

__version__ = '0.0.1'

__all__ = 'sh', 'bash'


def sh(*words):
    if not words: return ''
    return ''.join(("'", "' '".join(
        word.replace("'", "'\\''")
        for word in map(str, words)), "'"))

bash = sh


if __name__ == '__main__':
    import sys
    print(sh(*sys.argv[1:]), end='')
