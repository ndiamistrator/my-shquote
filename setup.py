from setuptools import setup
from shquote import __version__

setup(
    name='shquote',
    version=__version__,
    author='ndiamistrator',
    author_email='ndiamistrator@gmail.com',
    url='https://gitlab.com/ndiamistrator/my-shquote',
    download_url='https://gitlab.com/ndiamistrator/my-shquote.git',
    py_modules=['shquote'],
)
